

a = 123
b = 456


def print_id():
    print("id of a is = ",id(a))
    print("id of b is = ", id(b))
print_id()
a=b
print_id()
print( a is b)

a += 2 # python first creates "int 2" then add 2 in "a" and then assign variable "a" to the answer
print(id(a)) #because of above reason, id is differents

#value vs identity Equality
p = [1,4,7,4]
q = [1,4,7,4]
print("if p value is equal to q?: ", (p == q)) #p value is same as q
print("if p identity is equal to q?:", (p is q)) # p identity is not equal to q because both are referenced to different variables